
import UserList from './components/UserList.vue'
import UpdateUser from './components/UpdateUser.vue'

export const routes = [
    {path: '/' , name: 'UserList', component : UserList},
    {path: '/updateuser' , name: 'UpdateUser', component : UpdateUser}
];